﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MovingObject
{
	public static PlayerMove instance = null;
	private Animator animator;
	public GameObject particleWalk;
	
    // Start is called before the first frame update
    protected override void Start()
    {
		if (instance == null)
			//if not, set instance to this
			instance = this;
		//If instance already exists and it's not this:
		else if (instance != this)
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);
        animator = GetComponent<Animator>();
		base.Start();
    }

	private void OnDisable()
	{
		
	}
	
	private IEnumerator WaitToGrow()
	{
		while(moving){
			yield return null;
		}
		Kraken.instance.Grow();
		Kraken.instance.ReloadGui();
		GameManager.instance.playerCanPlay = true;
	}
	
	protected override void AttemptMove<T>(int xDir, int yDir)
	{
		//Hit will store whatever our linecast hits when Move is called.
		RaycastHit2D hit;
		//Set canMove to true if Move was successful, false if failed.
		bool canMove = Move(xDir, yDir, out hit);
		if(canMove){
			Instantiate(particleWalk,transform.position, Quaternion.identity);
			GameManager.instance.playerCanPlay = false;
			StartCoroutine(WaitToGrow());
			GameObject.Find("Main Camera").GetComponent<ShakeBehavior>().SmallShake();
		}
		//Check if nothing was hit by linecast
		if(hit.transform == null)
			return;
		//Get a component reference to the component of type T attached to the object that was hit
		T hitComponent = hit.transform.GetComponent <T> ();
		//If canMove is false and hitComponent is not equal to null, meaning MovingObject is blocked and has hit something it can interact with.
		if(!canMove && hitComponent != null){
			//Call the OnCantMove function and pass it hitComponent as a parameter.
			OnCantMove (hitComponent);
		}
	}

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.gameOver || !GameManager.instance.playerCanPlay) return;
		
		int horizontal = 0;
		int vertical = 0;
		
		horizontal = (int) Input.GetAxisRaw("Horizontal");
		vertical = (int) Input.GetAxisRaw("Vertical");
		
		if(horizontal != 0){
			vertical = 0;
		}
		
		if(horizontal != 0 || vertical != 0){
			AttemptMove<Tentacle>(horizontal,vertical);
		}
    }
	
	protected override void OnCantMove<T>(T component)
	{
		if(typeof(T) == typeof(Tentacle)){
			Tentacle t = (Tentacle)(object)component;
			if(t.next != null){
				t.CutTentacle();
			}else{
				Vector2 start = transform.position;
				Vector2 tentacle = t.transform.position;
				Vector2 diff = tentacle-start;
				int dir = 0;
				if(diff.x == 1){
					dir = 1;
				}else if(diff.y == 1){
					dir = 2;
				}else if(diff.x == -1){
					dir = 3;
				}
				if((dir == (t.direction+1)%4)||(dir==(t.direction+3)%4)){
					t.setDirection(dir);
				}
			}
		}
	}
	
	private void OnTriggerEnter2D(Collider2D other){
	}
}
