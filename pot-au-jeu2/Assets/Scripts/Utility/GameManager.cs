﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;       //Allows us to use Lists. 
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class GameManager : MonoBehaviour
{
	public bool gameOver = false;
	public bool success = false;
	public static GameManager instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
	public AudioSource audioSource;
	public AudioClip theme;
	public AudioClip musicMenu;
	public bool inMenu = true;
	public GameObject victoryObject;
	public GameObject failureObject;
	[HideInInspector] public bool playerCanPlay = true;
	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
			
			//if not, set instance to this
			instance = this;
		
		//If instance already exists and it's not this:
		else if (instance != this)
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);
		
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
	}
	
	void InitGame()
	{
		gameOver = false;
		success = false;
		playerCanPlay = true;
	}
	
	public void GoToMenu()
	{
		audioSource.Stop();
		audioSource.clip = musicMenu;
        audioSource.Play();
		inMenu = true;
		LevelManager.instance.GoToMenu();
	}
	
	void StartGame()
	{
		inMenu = false;
		audioSource.Stop();
		audioSource.clip = theme;
        audioSource.Play();
		InitGame();
		LevelManager.instance.InitGame();
	}
	
	public void GameOver()
	{
		SoundManager.instance.GameOver();
		gameOver = true;
		Vector2 pos = GameObject.Find("Main Camera").transform.position;
		Instantiate(failureObject, new Vector3(pos.x,pos.y, -2) ,Quaternion.identity);
		GameObject.Find("Main Camera").GetComponent<ShakeBehavior>().BigShake();
	}
	
	public void Success()
	{
		SoundManager.instance.Win();
		success = true;
		gameOver = true;
		Vector2 pos = GameObject.Find("Main Camera").transform.position;
		Instantiate(victoryObject, new Vector3(pos.x,pos.y, -3) ,Quaternion.identity);
		GameObject.Find("Main Camera").GetComponent<ShakeBehavior>().BigShake();
	}
	
	//Update is called every frame.
	void Update()
	{
		if(!inMenu){
			bool action = Input.GetButtonDown("Action");
			bool exit = Input.GetButtonDown("Exit");
			if(action){
				if(success){
					LevelManager.instance.NextLevel();
				}else{
					LevelManager.instance.Reload();
				}
				InitGame();
			}
			if(exit){
				GoToMenu();
			}
		}else{
			bool action = Input.GetButtonDown("Action");
			if(action){
				StartGame();
			}
		}
	}
}