﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EditorGrid : MonoBehaviour {
	public bool active = true;
	public float cell_size = 1.28f;
	private float x, y, z;
	
	void Start() {
		x = 0f;
		y = 0f;
		z = 0f;

	}
	
	void Update () {
		if (!Application.IsPlaying(gameObject))
        {
			x = Mathf.Round(transform.position.x / cell_size) * cell_size;
			y = Mathf.Round(transform.position.y / cell_size) * cell_size;
			z = transform.position.z;
			transform.position = new Vector3(x, y, z);
        }
	}
	
}