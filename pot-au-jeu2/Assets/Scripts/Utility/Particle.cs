﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
		Animator m_Animator = gameObject.GetComponent<Animator>();
		AnimatorClipInfo[] m_CurrentClipInfo = m_Animator.GetCurrentAnimatorClipInfo(0);
		float m_CurrentClipLength = m_CurrentClipInfo[0].clip.length;
        Destroy(gameObject, m_CurrentClipLength);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
