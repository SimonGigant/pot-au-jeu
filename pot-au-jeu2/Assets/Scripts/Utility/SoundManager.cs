﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
	public static SoundManager instance = null;
	public AudioClip cut;
	public AudioClip gameOver;
	public AudioClip win;	
	public AudioSource audioSource;
    void Awake()
	{
		//Check if instance already exists
		if (instance == null)
			
			//if not, set instance to this
			instance = this;
		
		//If instance already exists and it's not this:
		else if (instance != this)
			
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);
	}

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void Cut()
	{
		audioSource.clip = cut;
        audioSource.Play();
		//GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ScreenShake();
	}
	
	public void GameOver()
	{
		audioSource.clip = gameOver;
        audioSource.Play();
	}
	
	public void Win()
	{
		audioSource.clip = win;
        audioSource.Play();
	}
}
