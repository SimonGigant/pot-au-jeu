﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    // Call avec une ligne dans ce genre la
    // GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>().ScreenShake();
public class ShakeBehavior : MonoBehaviour
{
    // Transform of the GameObject you want to shake
    private Transform camTransform;

    // Desired duration of the shake effect
    private float shakeDuration = 0f;

    // A measure of magnitude for the shake. Tweak based on your preference
    private float shakeMagnitude = 0.07f;

    // A measure of how quickly the shake effect should evaporate
    private float dampingSpeed = 0.9f;

    // The initial position of the GameObject
    Vector3 initialPosition;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = gameObject.transform;
        }
    }

    void OnEnable()
    {
        initialPosition = gameObject.transform.localPosition;
    }

    void Update()
    {
        if (shakeDuration > 0)
        {
            gameObject.transform.localPosition = initialPosition + Random.insideUnitSphere * shakeMagnitude;

            shakeDuration -= Time.deltaTime * dampingSpeed;
        }
        else
        {
            shakeDuration = 0f;
            gameObject.transform.localPosition = initialPosition;
        }
    }

    public void SmallShake()
    {
        shakeDuration = 0.03f;
		shakeMagnitude = 0.01f;
		dampingSpeed = 1.2f;
    }

    public void BigShake()
    {
        shakeDuration = 0.15f;
		shakeMagnitude = 0.07f;
		dampingSpeed = 0.9f;
    }
}
