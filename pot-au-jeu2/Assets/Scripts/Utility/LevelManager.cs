﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;       //Allows us to use Lists. 
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	public static LevelManager instance = null;
	public string[] levels;
	public string menu;
	public int currentLevel = 0;
	//Awake is always called before any Start functions
	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
			
			//if not, set instance to this
			instance = this;
		
		//If instance already exists and it's not this:
		else if (instance != this)
			
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);    
		
		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
	}
	
	//Initializes the game for each level.
	public void InitGame()
	{
		currentLevel = 0;
		SceneManager.LoadScene(levels[currentLevel]);
	}
	
	public void GoToMenu()
	{
		SceneManager.LoadScene("menu");
		currentLevel = 0;
	}
	
	public void Reload()
	{
		SceneManager.LoadScene(levels[currentLevel]);
	}
	
	public void NextLevel()
	{	
		if(currentLevel+1 < levels.Length){
			++currentLevel;
			SceneManager.LoadScene(levels[currentLevel]);
		}else{
			GameManager.instance.GoToMenu();
		}
	}
	
	//Update is called every frame.
	void Update()
	{
	}
}