﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tentacle : MonoBehaviour
{
	public int direction;
	public Tentacle next;
	private Tentacle previous;
	private int initialDirection;
	public GameObject objectToCreate;
	public GameObject guiScissorsToCreate;
	public Animator animator;
	private BoxCollider2D boxCollider;      //The BoxCollider2D component attached to this object.
	private Rigidbody2D rb2D;               //The Rigidbody2D component attached to this object.
	public LayerMask blockingLayer;
	public bool cut;
	public int idTentacle;
	private Scissor guiScissors;
	
    // Start is called before the first frame update
    void Start()
    {
		initialDirection = direction;
		cut = false;
        Kraken.instance.Add(this);
		setDirection(direction);
		transform.Rotate (Vector3.forward * 90 * (direction+1));
		//Get a component reference to this object's BoxCollider2D
		boxCollider = GetComponent <BoxCollider2D> ();
		//Get a component reference to this object's Rigidbody2D
		rb2D = GetComponent <Rigidbody2D> ();
		guiScissors = null;
    }

    // Update is called once per frame
    void Update()
    {
		
    }
	
	public void ReloadGui()
	{
		bool adjacent = false;
		Vector2 playerPos = PlayerMove.instance.transform.position;
		Vector2 thisPos = transform.position;
		adjacent = Vector2.Distance(playerPos,thisPos) < 1.3;
		
		if(next != null && adjacent){
			if(guiScissors==null){
				guiScissors = Instantiate(guiScissorsToCreate, new Vector3(transform.position.x, transform.position.y, -2),Quaternion.identity).GetComponent<Scissor>();
			}
		}else{
			if(guiScissors!=null){
				guiScissors.Destroy();
				guiScissors = null;
			}
		}
	}
	
	public void setDirection(int d)
	{
		if(cut){
			return;
		}
		direction = d%4;
		int pDir;
		if(previous != null){
			pDir = previous.direction;
		}else{
			pDir = initialDirection;
		}
		if(pDir == (direction + 1)%4){
			animator.SetInteger("angleState",-1);
		}else if(pDir == (direction + 3)%4){
			animator.SetInteger("angleState",1);
		}
	}
	
	public void Grow()
	{
		if(cut){
			cut = false;
			animator.SetBool("isCut",false);
			return;
		}
		int xOffset = 0;
		int yOffset = 0;
		switch(direction){
			case 0:{yOffset=-1;break;}
			case 1:{xOffset=1;break;}
			case 2:{yOffset=1;break;}
			case 3:{xOffset=-1;break;}
		}
		if(next == null){
			//Store start position to move from, based on objects current transform position.
            Vector2 start = transform.position;
            
            // Calculate end position based on the direction parameters passed in when calling Move.
            Vector2 end = start + new Vector2 (xOffset, yOffset);
            
            //Disable the boxCollider so that linecast doesn't hit this object's own collider.
            boxCollider.enabled = false;
            
            //Cast a line from start point to end point checking collision on blockingLayer.
            RaycastHit2D hit = Physics2D.Linecast (start, end, blockingLayer);
            
            //Re-enable boxCollider after linecast
            boxCollider.enabled = true;
            
            //Check if anything was hit
            if(hit.transform == null)
            {
				next = CreateTentacle(xOffset,yOffset);
				next.previous = this;
				next.idTentacle = idTentacle;
				animator.SetBool("hasNext",true);
				animator.SetBool("isCut",false);
            }else{
				Obstacle t = hit.transform.GetComponent<Obstacle>();
				if(t!=null && t.dangerousForKraken){
					next = CreateTentacle(xOffset,yOffset);
					next.previous = this;
					next.idTentacle = idTentacle;
					animator.SetBool("hasNext",true);
					animator.SetBool("isCut",false);
					GameManager.instance.GameOver();
				} else if(t!=null && t.goal) {
					next = CreateTentacle(xOffset,yOffset);
					next.previous = this;
					next.idTentacle = idTentacle;
					animator.SetBool("hasNext",true);
					animator.SetBool("isCut",false);
					GameManager.instance.Success();
				}
			}
		}
	}
	
	public Tentacle CreateTentacle(int xOffset, int yOffset)
	{
		Tentacle res = Instantiate(objectToCreate,new Vector2((int)transform.position.x+xOffset,(int)transform.position.y+yOffset), Quaternion.identity).GetComponent<Tentacle>();
		return res;
	}

	public void CutTentacle()
	{
		SoundManager.instance.Cut();
		GameObject.Find("Main Camera").GetComponent<ShakeBehavior>().BigShake();
		cut = true;
		if(previous != null){
			direction = previous.direction;
		}else{
			direction = initialDirection;
		}
		if(next != null){
			next.Destroy();
			next = null;
		}
		animator.SetBool("hasNext",false);
		animator.SetBool("isCut",true);
		animator.SetInteger("angleState",0);
		Kraken.instance.ReloadGui();
	}
	
	private void Destroy()
	{
		if(guiScissors!=null){
			guiScissors.Destroy();
			guiScissors = null;
		}
		if(next != null){
			next.Destroy();
		}
		Kraken.instance.Remove(this);
		Destroy(gameObject);
	}
}