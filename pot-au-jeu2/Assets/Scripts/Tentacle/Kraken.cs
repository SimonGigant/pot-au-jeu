﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kraken : MonoBehaviour
{
	public List<Tentacle> list;
	public GameObject objectToCreate;
	public static Kraken instance = null;
	private Tentacle[] tentacles;
	private int count;
	
    // Start is called before the first frame update
    void Start()
    {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy(gameObject);
		count=0;
        list = new List<Tentacle>();
		tentacles = new Tentacle[4];
		tentacles[0] = Instantiate(objectToCreate,new Vector2((int)transform.position.x,(int)transform.position.y-1), Quaternion.identity).GetComponent<Tentacle>();
		tentacles[1] = Instantiate(objectToCreate,new Vector2((int)transform.position.x+1,(int)transform.position.y), Quaternion.identity).GetComponent<Tentacle>();
		tentacles[2] = Instantiate(objectToCreate,new Vector2((int)transform.position.x,(int)transform.position.y+1), Quaternion.identity).GetComponent<Tentacle>();
		tentacles[3] = Instantiate(objectToCreate,new Vector2((int)transform.position.x-1,(int)transform.position.y), Quaternion.identity).GetComponent<Tentacle>();
		for(int i = 0; i < 4; ++i){
			tentacles[i].direction = i;
			tentacles[i].idTentacle = i;
		}
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	public void Add(Tentacle t)
	{
		list.Add(t);
	}
	
	public void Remove(Tentacle t)
	{
		list.Remove(t);
	}
	
	public void ReloadGui()
	{
		list.ForEach(el => el.ReloadGui());
	}
	
	private void AttemptGrow(Tentacle el)
	{
		if(count==el.idTentacle){
			el.Grow();
			el.ReloadGui();
		}
	}
	
	public void Grow()
	{
		list.ForEach(el => AttemptGrow(el));
		count = (count+1)%4;
	}
}
